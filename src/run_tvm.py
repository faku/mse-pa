###############################################################################
# File: tvm.py
#
# Created by: Lucas Elisei <lucas.elisei@master.hes-so.ch>
# Created on: 21.05.2019
###############################################################################

# Imports
import numpy as np
import os
import random
import sys
import time

from matplotlib import pyplot as plt
from PIL import Image

# TVM imports
import tvm
import nnvm.compiler
import nnvm.testing
import vta
import vta.testing

from tvm import autotvm
from tvm.autotvm.tuner import RandomTuner
from tvm.contrib.util import tempdir
import tvm.contrib.graph_runtime as runtime

# Get VTA environment.
env = vta.get_env()

# Get network workload.
def get_network(network: str,
                batch_size: int,
                num_classes: int,
                num_layers: int,
                image_shape: tuple,
                dtype: str):
    if (network == 'resnet'):
        net, params = nnvm.testing.resnet.get_workload(batch_size=batch_size,
                                                       num_classes=num_classes,
                                                       num_layers=num_layers,
                                                       image_shape=image_shape,
                                                       dtype=dtype)
    elif (network == 'mlp'):
        net, params = nnvm.testing.mlp.get_workload(batch_size=batch_size,
                                                    num_classes=num_classes,
                                                    image_shape=image_shape,
                                                    dtype=dtype)
    else:
        raise ValueError('Invalid network: {}'.format(network))

    return net, params

# Tasks tuning.
def tune_tasks(tasks,
               measure_option,
               n_trial=1000,
               early_stopping=None,
               log_filename='tuning.log',
               use_transfer_learning=True):

    # create tmp log file
    tmp_log_file = log_filename + ".tmp"
    if os.path.exists(tmp_log_file):
        os.remove(tmp_log_file)

    for i, tsk in enumerate(reversed(tasks)):
        prefix = "[Task %2d/%2d] " % (i+1, len(tasks))

        # Create tuner.
        tuner_obj = RandomTuner(tsk)

        if use_transfer_learning:
            if os.path.isfile(tmp_log_file):
                tuner_obj.load_history(autotvm.record.load_from_file(tmp_log_file))

        # do tuning
        tuner_obj.tune(n_trial=min(n_trial, len(tsk.config_space)),
                       early_stopping=early_stopping,
                       measure_option=measure_option,
                       callbacks=[
                           autotvm.callback.progress_bar(n_trial, prefix=prefix),
                           autotvm.callback.log_to_file(tmp_log_file)])

    # pick best records to a cache file
    autotvm.record.pick_best(tmp_log_file, log_filename)
    os.remove(tmp_log_file)

# Tune and evaluate network.
def tune_and_evaluate(tuning_opt, do_tuning=True):
    # Get network workload.
    print('Getting network workload...')
    net, params = get_network(network, batch_size, num_classes, num_layers, image_shape, dtype)

    data_shape = {'data': (batch_size, ) + image_shape}

    # Extract tasks.
    if (do_tuning == True):
        print('Extracting tasks...')
        tasks = autotvm.task.extract_from_graph(net,
                                                shape=data_shape,
                                                dtype=dtype,
                                                target=target,
                                                symbols=symbols,
                                                target_host=target_host)
        print('Tuning tasks...')
        tune_tasks(tasks, **tuning_opt)
    else:
        print('Extracting tasks from log file...')

    # Compile kernels with history best records (using log file).
    with autotvm.apply_history_best(tuning_opt['log_filename']):
        print('Compiling kernels...')
        with nnvm.compiler.build_config(opt_level=3):
            graph, lib, params = nnvm.compiler.build(net,
                                                     target=target,
                                                     shape=data_shape,
                                                     dtype=dtype,
                                                     params=params,
                                                     target_host=target_host)

        if (env.TARGET == 'pynq'):
            print('Uploading to PYNQ board...')
            tmp = tempdir()
            filename = 'net_pynq.tar'
            lib.export_library(tmp.relpath(filename))

            # Upload module to the board.
            remote_start = time.time()
            remote = autotvm.measure.request_remote(device_key, host=rpc_host, port=rpc_port, timeout=10000)
            remote.upload(tmp.relpath(filename))
            rlib = remote.load_module(filename)
            remote_time = time.time() - remote_start
            print('Module uploaded to board in {0:.2f}s'.format(remote_time))

            ctx = remote.context(str(target), 0)
            module = runtime.create(graph, rlib, ctx)
        elif (env.TARGET == 'sim'):
            ctx = tvm.cpu()
            module = runtime.create(graph, lib, ctx)

        # Create random data for inference.
        data = tvm.nd.array((np.random.uniform(size=data_shape['data'])).astype(dtype))

        # Upload data and parameters to target.
        module.set_input('data', data)
        module.set_input(**params)

        # Evaluate inference.
        print('Evaluating inference time cost...')
        ftimer = module.module.time_evaluator('run', ctx, number=1, repeat=100)
        prof_res = np.array(ftimer().results) * 1000    # Convert to milliseconds.

        print('Mean inference time (std dev): {:.2f} ms ({:.2f} ms)'.format(np.mean(prof_res), np.std(prof_res)))

# Constants
network = 'mlp'
batch_size = 1
num_classes = 10
num_layers = 50
image_shape = (1, 28, 28)
dtype = 'float32'
symbols = (nnvm.sym.conv2d, nnvm.sym.dense)

# Compilation variables.
if (env.TARGET == 'sim'):       # CPU
    device      = 'vtacpu'
    target      = 'llvm'
    target_host = 'llvm'
    tuning_opt = {
        'log_filename': '{}_{}.log'.format(network, env.TARGET),

        'n_trial': 10,
        # 'early_stopping': 80,

        'measure_option': autotvm.measure_option(
            builder=autotvm.LocalBuilder(),
            runner=autotvm.LocalRunner(number=10, repeat=10, min_repeat_ms=1000)
        )
    }

elif (env.TARGET == 'pynq'):    # Pynq board
    device      = 'vta'
    target      = 'llvm -device=arm_cpu -target=armv7-none-linux-gnueabihf'
    target_host = 'llvm -mtriple=armv7-none-linux-gnueabihf -mcpu=cortex-a9 -mattr=+neon'
    rpc_host    = '0.0.0.0'
    rpc_port    = 9191
    device_key  = 'pynq'
    tuning_opt = {
        'log_filename': '{}_{}.log'.format(network, env.TARGET),

        'n_trial': 10,
        #'early_stopping': 80,

        'measure_option': autotvm.measure_option(
            builder=autotvm.LocalBuilder(),
            runner=autotvm.RPCRunner(
                key=device_key, host=rpc_host, port=rpc_port,
                number=10, repeat=10,
                timeout=10,
            ),
        ),
    }

else:                           # Unknown target
    raise ValueError('Unknown target: {}'.format(env.TARGET))

# Define how many threads TVM can use.
num_threads = 4
os.environ['TVM_NUM_THREADS'] = str(num_threads)

tune_and_evaluate(tuning_opt, do_tuning=True)