\section{Choosing Neural Network models to evaluate}

Now that we defined TVM and LeFlow as the tools we will evaluate, we have to
choose which \acrlong{nn} models are suitable for implementation and evaluation.

We want to choose two \acrshort{nn} models: a simple and a more complex one. The
reason for this is so that we can evaluate tools performance on two different
complexities along with the difficulty to implement such complex models.

Benchmarking tools for measuring performance of \acrlong{ml} software frameworks
already exist. They are interesting for measuring the number of operations per
second required for doing inference on a fitted \acrlong{nn}.

\begin{itemize}
\item   MLPerf is a benchmark suite for measuring performance of \acrshort{ml}
        software frameworks, hardware accelerators and cloud platforms \cite{mlperf}.
        It is a community-driven open-source project supported by a lot of companies.
\item   Deep500 is a Python library that enables fair comparison of deep learning
        frameworks, algorithms, libraries and techniques \cite{arxiv:190110183}.
        It is developed by a team from the Department of Computer Science of ETH
        Zurich.
\end{itemize}

These are really interesting benchmarking tools and we can only encourage you to
go look further. However, in this paper, we focus on both implementation and output
performance and using these tools would require a lot of useless efforts.

As stated before, we want to choose two \acrlong{nn} models with different
complexities. Two models are widely used when introducing people to \acrlong{ml}
and \acrlong{ai}: a simple \acrfull{mlp} and ResNet.

\subsection{Simple Multi-Layer Perceptron}

One of the simplest and common \acrshort{nn} models is the single \acrlong{mlp}.
We presented its basic architecture with the figure \ref{fig:simple-mlp}.

We want to create a classifier that recognizes handwritten digits using the MNIST
dataset \cite{mnist}.

MNIST contains 70000 images of handwritten digits: 60000 for training and 10000
for testing. The images are grayscale, 28x28 pixels and centered.

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=0.5\linewidth]{figures/mnist_selection.png}
    \captionof{figure}{Random selection of MNIST digits \cite{medium-mnist}.}
\end{minipage}

The model is a bit more complex than the \acrshort{mlp} presented before. This
model has 4 layers:

\begin{enumerate}
\item   the input layer, with 784 entries (one for each pixel of the image);
\item   the first hidden layer, with 128 entries, uses \acrshort{relu} as activation
        function;
\item   the second hidden layer, with 64 entries, also uses \acrshort{relu} as
        activation function;
\item   the output layer, with 10 outputs, classifies the input image.
\end{enumerate}

Below is a graphical representation of the model:

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=0.6\linewidth]{figures/mlp_structure.png}
    \captionof{figure}{Simple \acrlong{mlp}}
\end{minipage}

The first layer has $784 * 128 \text{ (weigths)} + 128 \text{ (biases)} = 100480$
tunable parameters, the second layer has $128*64 + 64 = 8256$ tunable parameters
and the third one has $64*10 + 10 = 650$ tunable parameters. \\
Thus, the model has $100480 + 8256 + 650 = 109386$ total parameters.

\subsection{ResNet-50 v1.5}

\acrfull{resnet} is an artificial \acrlong{nn}. It is used to achieve human-level
image classification.

To train, it uses the ImageNet dataset \cite{imagenet}. This dataset contains
millions of images belonging to more than 20000 categories. Each image is 224x244 and
centered.

The ResNet-50 v1.5 model is a modified version of the original ResNet-50 v1 model \cite{resnet50-v15}.
The difference between v1 and v1.5 makes the most recent version more accurate (\textasciitilde0.5\%)
but comes with a small performance drawback (\textasciitilde5\% imgs/sec).

\begin{minipage}{\linewidth}
    \centering
    \begin{minipage}{0.4\linewidth}
        \centering
        \includegraphics[width=\linewidth]{figures/resnet_dog.png}
    \end{minipage}
    \hfill
    \begin{minipage}{0.55\linewidth}
        \begin{minted}[linenos=false]{python}
[
    ('n02106662', 'German_shepherd', 0.33396399),
    ('n02093754', 'Border_terrier',  0.29242834),
    ('n02105162', 'malinois',        0.16545239),
    ('n02096051', 'Airedale',        0.04790553),
    ('n02106550', 'Rottweiler',      0.03723163)
]
        \end{minted}
    \end{minipage}
    \captionof{figure}{ResNet-50 example output \cite{resnet-example}.}
\end{minipage}

The model has more than 25 million total tunable parameters \cite{arxiv:160507146}, making it way more
complex than the \acrshort{mlp} model previously introduced.