\section{Introduction}

In the 1950s, pioneering \acrfull{ml} research has been conducted using simple
algorithms. At the time, there was not enough computational power and data
available to explore further the possibilities that \acrlong{ml} could offer and,
for some years, research was quasi-inexistant.

However, in the recent years, with the Big Data era and the computational power
growing exponentially, \acrlong{ml} started to rise from the ashes and more and
more companies began using it for a plethora of applications (\textit{e.g.}
image classification, object detection and speech recognition).

This performance comes at the price of a large computational cost as
\acrfullpl{cnn} require up to 38 GOP/s to classify a single frame \cite{arxiv-14091556}.
As a result, dedicated hardware is required to accelerate their execution.
\acrfullpl{gpu} are the most widely used platform to implement \acrshortpl{cnn}
as they offer the best performance in terms of pure computational throughput,
reaching up to 11 TFLOP/s \cite{can-fpgas-beat-gpus}. Nevertheless, in terms of
power consumption, \acrfullpl{fpga} are known to be more energy efficient than
\acrshortpl{gpu}. Thus, numerous \acrshort{fpga}-based \acrshort{cnn} accelerators
have been proposed, targeting both \acrfull{hpc} data-centers and embedded
applications \cite{arxiv-180601683}.

\subsection{Study aim and objectives}

In this study, we are looking for frameworks that allow translating a \acrfull{nn}
model to synthesizable hardware. We aim at benchmarking the full development and
deployment process of any \acrshort{nn} and evaluating the output performances,
especially the inference.

To achieve this aim, the following objectives have been identified:

\begin{enumerate}
\item   Review existing frameworks;
\item   Pick the most interesting candidates of the existing frameworks;
\item   Choose two neural network models with which we will evaluate the previously picked tools;
\item   Implement those two models and evaluate their convenience to do so;
\item   Measure inference performance;
\item   Compare results;
\item   Conclude.
\end{enumerate}

\subsection{Introduction to Machine Learning}

\acrfull{ml} is a data analytics technique that teaches computers to learn as
humans or animals: from experience. \acrlong{ml} algorithms use computational
methods to \textit{learn} information directly from data without relying on a
predetermined equation as a model \cite{what-is-ml}.

\acrlong{ml} uses two types of techniques: \textit{supervised learning}, which
trains a model on known input and output data so that it can predict future
outputs; and \textit{unsupervised learning}, which finds hidden patterns by
itself in input data without output data.

\textit{Supervised learning} uses classification and regression techniques to
develop predictive models. \\
Classification techniques predict discrete responses, they classify input data
into categories. Typical applications include medical imaging, speech recognition
and credit scoring. \\
Regression techniques predict continuous responses, such as changes in
temperature or fluctuations in power demand. Typical applications include
electricity load forecasting and algorithmic trading.

\textit{Unsupervised learning} finds hidden patterns or intrinsic structures in
data. It is used to draw inferences from datasets consisting of input data
without labelled responses. \\
Clustering is the most common unsupervised learning technique. It is used for
exploratory data analysis to find hidden patterns or groupings in data.
Applications for cluster analysis include gene sequence analysis, market
research and object recognition.

In this study, we will talk about \acrshort{ml} algorithms based on supervised
learning only.

\subsubsection*{What is a \acrlong{nn}?}

To get started, we first have to define what is a \textit{perceptron}. A
perceptron is an artificial neuron that takes one or several inputs and produces
a single output, just like human neurons.

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=0.4\linewidth]{figures/perceptron.png}
    \captionof{figure}{A perceptron with three inputs \cite{nielsen-neural}.}
\end{minipage}

In the example shown above, the perceptron has three inputs $x_1$, $x_2$ and
$x_3$. In general, it could have more or fewer inputs. Each input has a weight
($w_1$, $w_2$, $w_3$) expressing its importance relative to the others. The
neuron's output, $0$ or $1$, is determined by whether the weighted sum
$\sum_i w_i x_i$ is less than or greater than a \textit{threshold} value. To put
it in more algebraic terms:
$$\text{output} =
    \begin{cases}
        0 & \text{if } \sum_i w_i x_i \leq \text{threshold} \\
        1 & \text{if } \sum_i w_i x_i > \text{threshold}
    \end{cases}$$

Obviously, the perceptron is not a complete model of human decision-making but it
seems plausible that a complex network of perceptrons could make quite subtle
decisions. This is what we call a \acrlong{nn}.

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=0.5\linewidth]{figures/neural_network.png}
    \captionof{figure}{Simple \acrfull{mlp} network \cite{nielsen-neural}.}
    \label{fig:simple-mlp}
\end{minipage}

A \acrfull{nn} is composed of several layers (three in the example shown above).
The leftmost layer in this network is called the \textit{input layer}, and the
neurons within the layer are called \textit{input neurons}. \\
The rightmost layer, or \textit{output layer}, contains the \textit{output
neurons}, or, as in this case, a single output neuron. \\
The middle layer is called a \textit{hidden layer} since the neurons in this
layer are neither inputs nor outputs. The network above has just a single hidden
layer but some networks have multiple hidden layers.

\subsubsection*{Training, validating and testing a \acrlong{nn}}

The data used to build the final model usually comes from multiple datasets. In
particular, three datasets are commonly used in different stages of the creation
of the model \cite{training-testing-nn}.

The model is initially fit on a \textit{training dataset}. The \acrshort{nn} is
run with the training dataset and produces a result, which is then compared with
the \textit{target} (or \textit{label}), for each input of the training dataset.
Based on the result of the comparison, the parameters of the model are adjusted.

Successively, the fitted model is used to predict the responses for the
observations in a second dataset, called the \textit{validation dataset}.
Validation datasets can be used for stopping the training when the error on the
validation dataset increase, as this is a sign of overfitting to the training
dataset.

Finally, the \textit{test dataset} is a dataset used to provide an unbiased
evaluation of the final model predictions. A test dataset is independent of the
training dataset.

\subsubsection*{Inference}

Once the \acrlong{nn} has been trained, it is ready for inference -- to classify,
recognize and process new inputs. For instance, the voice of someone talking to
Siri on his iPhone is sent to an Apple server on which a \acrshort{cnn} trained
for speech recognition has been deployed.

\subsubsection*{Remarkable works and success stories}

\begin{itemize}
\item   In 2019, Yoshua Bengio, Geoffrey Hinton and Yann LeCun have been awarded
        the Turing Award for their conceptual and engineering breakthroughs that
        have made \acrfullpl{dnn} a critical component of computing \cite{turing-award}.

\item   Fei-Fei Li was the leading scientist and principal investigator of ImageNet
        \cite{imagenet}, an image database with more than 14 million images
        classified over 22,000 categories. This dataset was crucial for
        \acrshortpl{cnn} evolution.

\item   Voice recognition services such as \textit{Siri}, \textit{OK Google} or
        \textit{Amazon Echo} work well thanks to \acrlong{ml} \cite{siri-ml}.

\item   Self-driving cars (\textit{e.g.} Tesla Autopilot \cite{tesla-autopilot}).

\item   Netflix saves up to 1 billion dollars each year thanks to its AI
        algorithms \cite{netflix}.

\item   IBM's project Debater is the first \acrshort{ai} system that can debate
        humans on complex topics \cite{debater}.

\item   In 2017, Google created AutoML, an \acrshort{ai} that is capable of
        generating its own \acrshort{ai}s, outperforming all of its human-made
        counterparts \cite{automl}.
\end{itemize}