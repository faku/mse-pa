\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Study aim and objectives}{1}{subsection.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Introduction to Machine Learning}{1}{subsection.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Existing frameworks}{4}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}DnnWeaver 2.0}{4}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}FPGA Caffe}{5}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}NVDLA}{5}{subsection.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}hls4ml}{6}{subsection.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}TVM}{7}{subsection.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6}LeFlow}{8}{subsection.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7}nGraph}{8}{subsection.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.8}HeteroCL}{9}{subsection.2.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.9}Comparison}{10}{subsection.2.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.10}Summary}{11}{subsection.2.10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Choosing Neural Network models to evaluate}{12}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Simple Multi-Layer Perceptron}{12}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}ResNet-50 v1.5}{13}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Implementation}{14}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}TVM}{14}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.1}Installation}{14}{subsubsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.2}Board setup}{15}{subsubsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.3}Implementation}{16}{subsubsection.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.4}Results}{18}{subsubsection.4.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.5}Summary}{19}{subsubsection.4.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}LeFlow}{19}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.1}Installation}{19}{subsubsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2}Implementation}{20}{subsubsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.3}Results}{21}{subsubsection.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.4}Summary}{21}{subsubsection.4.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Conclusion}{22}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{References}{24}{section*.50}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Acronyms}{25}{section*.51}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Appendices}{26}{section*.53}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A}Authentication}{26}{Appendix.a.A}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B}\texttt {src/run\_leflow.py}}{27}{Appendix.a.B}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {C}\texttt {src/run\_tvm.py}}{29}{Appendix.a.C}
