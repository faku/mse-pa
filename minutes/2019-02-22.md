# 2019.02.22

### Projects

*   LeFlow: Open-source tool that maps TensorFlow numerical computation models
    to synthetisable hardware (verilog)
*   NVDLA: Nvidia's open-source Deep Learning Accelerator
*   VTA: Open-source customisable Deep Learning acceleration stack
*   OpenVino: IntelFPGA's solution
*   nGraph

### PA's goal

Comparative study of the existing solutions (state of the art). Then, make a
demo for each to identify the possible difficulties.

* [mlperf.org](mlperf.org): ML benchmark platform.
* The best option would be to use VTA ([tvm.ai](tvm.ai))

### Links

* https://www.scss.tcd.ie/~andersan/projects/live/triNNity.html
* http://dnnweaver.org/
* http://eyeriss.mit.edu/
* https://github.com/dicecco1/fpga_caffe
* http://nvdla.org/
* https://mlperf.org/
* https://github.com/hls-fpga-machine-learning/hls4ml
* https://github.com/Xilinx/ml-suite
* github TVM dmlc/tvm https://t.co/hKHUJfzAwE

* https://www.sigarch.org/artifact-evaluation-for-reproducible-quantitative-research/
* https://github.com/CSL-KU/firesim-nvdla
